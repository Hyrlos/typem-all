The game start when at least 2 players are connected. 30 seconds after that, a paragraph with some sentences is send to all the players. They have 2 minutes to write each words or as much as possible.
You win more point according to the speed for writing the words. But none point is earn if the word isn't write correctly.
When the timer is out, the winner is the player with most points than the others.

First launch the server
    `python server.py` 

launch at least 2 clients
    `python client.py pseudoPlayer1`
    `python client.py pseudoPlayer2`
    [. . .]
    `python client.py pseudoPlayerN`