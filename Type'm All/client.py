"""
/* Type'm All Client
 * By Thomas GEORGES, 015147796
 * Computer science : MASTER DEGREE
 * 12/10/2019 - 16/10/2019
 * Helsinki, FINLAND
 * 0.17 comments per line
 */
"""

import socket
import threading
import sys
import time

playerID = 0
fini = False
idMsgSend = 0
gameNumber = 0

# First exchange, the playerID is mandatory for the next
def clientConnexionTCP(HOST, PORT):
    global playerID
    sock = socket.socket()
    sock.connect((HOST, PORT))
    playerID = int(sock.recv(6666).decode())
    return playerID
# Connexion
def clientConnexionUDP():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return sock

# Build a 3 digit integer from a integer (1 => 001, 30 => 030)
def integer3Digit(integ):
    integ = str(integ)
    for i in range(3-len(integ)):
        integ = "0"+integ
    return integ

# Main function
def game(paragraphe, playerID,socket, addr):
    global idMsgSend
    global fini
    global gameNumber
    current_game = gameNumber
    print(paragraphe)
    text = paragraphe.split(" ")
    text.reverse()

    # While all the text is not write or the time is not over
    while text and not fini and current_game == gameNumber:
        word = text.pop()
        awnser = input(word+" ")
        if word == awnser:
            print("ok")
            # idMsg + 2 for score + current time
            msg = integer3Digit(idMsgSend)+integer3Digit(2)+integer3Digit(playerID)+str(int(time.time()))
            idMsgSend += 1
            sendMsg(msg, socket, addr)
        else:
            # if the word is false
            print("nok")

# send a message to the server (Triple FEC)
def sendMsg(msg, socket, addr):
    socket.sendto(msg.encode(), addr)
    socket.sendto(msg.encode(), addr)
    socket.sendto(msg.encode(), addr)


if __name__ == '__main__':
    HOST = "127.0.0.1" # Machine addr
    PORT_TCP = 6666
    PORT_UDP = 6667


    pseudo = "Unknown"
    if len(sys.argv) == 2:
        pseudo = sys.argv[1]

    print("Hello " + pseudo + ", ready to play to Type'm All?")


    # initialisation of the playerID
    playerID = clientConnexionTCP(HOST, PORT_TCP)

    # Send a message to the server to indicate that the player is ready
    udp_socket = clientConnexionUDP()

    # idMessgae, idTypeMessage, PlayerID, Pseudo
    # idTypeMessage = 0 For connexion
    msg = integer3Digit(idMsgSend)+integer3Digit(0)+integer3Digit(playerID)+pseudo
    sendMsg(msg, udp_socket, (HOST, PORT_UDP))

    idMsgSend += 1
    lastMsg = -1
    counter = 0
    content = ""
    score = []
    reward = ""
    filename = ""
    while True:
        data, addr = udp_socket.recvfrom(257)
        data = data.decode()
        # print(data)
        # if first receive
        if lastMsg < int(data[:3]):
            lastMsg = int(data[:3])
            #print(int(data[3:6]))
            # 1 <= timer
            if int(data[3:6]) == 1:
                print(data[6:])

            # 2 <= number of receive to catch each part
            if int(data[3:6]) == 2:
                counter = int(data[6:])
                content = ""

            # 3 <= portion of text
            if int(data[3:6]) == 3:
                content += data[6:]
                counter -= 1

                # When all the text is received
                if counter == 0 :
                    fini = False

                    # 1 => for time at the beggining
                    msg = integer3Digit(idMsgSend)+integer3Digit(1)+integer3Digit(playerID)+str(int(time.time()))
                    sendMsg(msg, udp_socket, (HOST, PORT_UDP))
                    idMsgSend += 1
                    gameNumber += 1
                    threading.Thread(target=game, args=(content,playerID,udp_socket,(HOST, PORT_UDP),)).start()

            # 4 <= end of game
            if int(data[3:6]) == 4:
                fini = True
                print("fini")
                counter = int(data[6:])
                score = []

            # 5 <= each score
            if int(data[3:6]) == 5:
                counter -= 1
                score.append(data[6:])

                if counter == 0 :
                    print("\n\n=============\n")
                    for i in range(len(score)):
                        print(score[i])

            # 6 : The winner
            if int(data[3:6]) == 6:
                if playerID == int(data[6:]):
                    print("YOU WIN !")
                    print("\n\n=============\n")
                    # if you are the winner, you can ask for a reward

                    msg = integer3Digit(idMsgSend)+integer3Digit(3)+integer3Digit(playerID)
                    # Cause some problem with sending bytes
                    # But work with non-binary fine
                    #sendMsg(msg, udp_socket, (HOST, PORT_UDP))


                else:
                    print("LOSER ¯\\_('-')_/¯")
                    print("\n\n=============\n")


            # 7 <= New Game Soon
            if int(data[3:6]) == 7:
                print(data[6:])

            # 8 <= number of part of reward
            if int(data[3:6]) == 8:
                print("part of reward:"+data[6:])
                counter = int(data[6:].split(" ")[0])
                filename = data[6:].split(" ")[1]
                reward = ""

            # 9 <= each part of the reward
            if int(data[3:6]) == 9:
                counter -= 1
                reward += data[6:]

                if counter == 0 :
                    print("We received a reward : " + filename)

                    fo = open("rewardsRecv/"+filename, "wb+")
                    fo.write(bytes(reward))
                    fo.close()
