"""
/* Type'm All Server
 * By Thomas GEORGES, 015147796
 * Computer science : MASTER DEGREE
 * 12/10/2019 - 16/10/2019
 * Helsinki, FINLAND
 * 0.22 comments per line
 */
"""

import socket
import threading
import time
import random
import os

playerID_current = 0
# 60s to write
timeGame = 60

# PlayerID, Pseudo, idLastMessageRecv, addr, idLastMessageSend
players = []

# PlayerID, IDplayersArray, time, score
gameArray = []

# Build a 3 digit integer from a integer (1 => 001, 30 => 030)
def integer3Digit(integ):
    integ = str(integ)
    for i in range(3-len(integ)):
        integ = "0"+integ
    return integ

# Connection
def servConnectionUDP(HOST, PORT):

    # create a socket object
    serversocket = socket.socket(
    	           socket.AF_INET, socket.SOCK_DGRAM)

    # bind to the port
    serversocket.bind((HOST, PORT))

    return serversocket

# All the TCP process
def servConnectionTCP(HOST, PORT):

    # create a socket object
    serversocket = socket.socket(
    	           socket.AF_INET, socket.SOCK_STREAM)

    # bind to the port
    serversocket.bind((HOST, PORT))

    # queue up to 5 requests
    serversocket.listen(5)

    # Open the connection, send the playerID, close the connection
    while True:
        clientsocket,addr = serversocket.accept()
        global playerID_current
        print("Got a connection from " +  str(addr) + " playerID = "+ str(playerID_current))
        clientsocket.send(str(playerID_current).encode('ascii'))
        clientsocket.close()
        playerID_current += 1

# check if the playerID exist
def playerExist(playerID):
        global players
        for i in range(len(players)):
            if players[i][0] == playerID:
                return True
        return False

# process for all UDP message received
def UDP_listen(sock):
        global players
        global gameArray
        global timeGame

        while True:
            data, addr = sock.recvfrom(6666)
            data = data.decode()



            # Switch, according to the id of the message
            # 0 = connection
            if(int(data[3:6]) == 0):
                # PlayerID, Pseudo, idLastMessageRecv, addr, idLastMessageSend
                if not playerExist(data[6:9]):
                    players.append((data[6:9],data[9:],0, addr, 0))
                    print("New player: "+data[9:])



            # 1 <=Time and initialisation for player
            if(int(data[3:6]) == 1):
                for i in range(len(players)):
                    # for the good player
                    if(players[i][0] == data[6:9]) and int(data[:3]) > int(players[i][2]):
                        #Maj player Array
                        # PlayerID, Pseudo, idLastMessageRecv, addr, idLastMessageSend
                        tuplePlayers = (players[i][0], players[i][1],players[i][2]+1,players[i][3], players[i][4])
                        players[i] = tuplePlayers

                        # initialisation
                        if(int(data[3:6]) == 1):
                            # PlayerID, IDplayersArray, time, score
                            gameArray.append((players[i][0], i, data[9:], 0))

            # 2 <= MAJ Score
            if(int(data[3:6]) == 2):
                # PlayerID, IDplayersArray, time, score
                for i in range(len(gameArray)):
                    if(data[6:9] == gameArray[i][0]) and int(data[:3]) > int(players[i][2]):

                        # 60 seconds to play (timeGame)
                        score = int(gameArray[i][3]) + timeGame - (int(data[9:]) - int(gameArray[i][2]))
                        # PlayerID, IDplayersArray, time, score
                        tupleGame = (gameArray[i][0], gameArray[i][1], gameArray[i][2], score)
                        gameArray[i] = tupleGame

                        indice = gameArray[i][1]

                        # PlayerID, Pseudo, idLastMessageRecv, addr, idLastMessageSend
                        tuplePlayers = (players[indice][0], players[indice][1] ,players[indice][2]+1,players[indice][3], players[indice][4])
                        players[i] = tuplePlayers

                        print(players[indice][1] + ": " + str(score) +" points")

            # 3 <= Ask for a reward
            if(int(data[3:6]) == 3):
                idPlayerArray = -1
                for i in range(len(players)):
                    if players[i][0] == data[6:9]:
                        idPlayerArray = i

                # receiveOnce
                tuplePlayers = (players[idPlayerArray][0], players[idPlayerArray][1],players[idPlayerArray][2]+1,players[idPlayerArray][3], players[idPlayerArray][4])
                players[idPlayerArray] = tuplePlayers

                files = os.listdir("rewards")
                filename = files[random.randint(0,len(files)-1)]
                path = "rewards/"+filename
                content = b""
                with open(path, "rb+") as f:
                    content += f.read()
                pick = splitSize(content.decode("ANSI"))

                # 8 => number of part of reward
                msg = "008"+str(len(pick)) + " " + filename
                players[idPlayerArray] = sendMsg(sock, msg, players[idPlayerArray])

                # 9 each part of the reward
                for i in range(len(pick)):
                    msg = "009"+ pick[i]
                    players[idPlayerArray] = sendMsg(sock, msg, players[idPlayerArray])

# Send a message to just one player
def sendMsg(socket, msg, player):
    msgi = integer3Digit(player[4]) + msg
    # PlayerID, Pseudo, idLastMessageRecv, addr, idLastMessageSend
    tuple = (player[0], player[1],player[2],player[3], int(player[4])+1)
    player = tuple
    socket.sendto(msgi.encode(), player[3])
    return player

# Send a message to all players
def broadcast(socket, msg, receivers):
    for i in range(len(receivers)):
        msgi = integer3Digit(receivers[i][4]) + msg
        #print(receivers[i])
        # PlayerID, Pseudo, idLastMessageRecv, addr, idLastMessageSend
        tuple = (receivers[i][0], receivers[i][1],receivers[i][2],receivers[i][3], int(receivers[i][4])+1)
        receivers[i] = tuple
        # Triple
        #print(msgi.encode())
        socket.sendto(msgi.encode(), receivers[i][3])
        socket.sendto(msgi.encode(), receivers[i][3])
        socket.sendto(msgi.encode(), receivers[i][3])
    return receivers

# Split on packet of 250Bytes
def splitSize(data):
    tmp=[]
    i = 0
    for i in range(0, len(data), 250):
        if(i <= len(data)):
            tmp.append(data[i:i+250])

    tmp.append(data[i+250:len(data)])
    return tmp

# TODO connection verification (somewhere)
def game(socket):
    global players
    global timeGame
    global gameArray
    # When at least two players are connected
    while len(players) < 2:
        time.sleep(1)

    print("Let the rock off begin! in 10s")

    t = 0
    playerReady = 0
    # Wait 30s for players
    # 10s is better
    while t < 10:
        if playerReady < len(players):
            # 1 => is for timer message
            msg = ("001Let the rock off begin! in "+ str(10 - t) + "s")
            players[playerReady:] = broadcast(socket, msg, players[playerReady:])
            playerReady = len(players)

        t += 1
        time.sleep(1)

    print("Come'on players, bring the thunder!")

    # re-initialisation game array
    gameArray = []
    playersCurrents = len(players)
    # The game start
    files = os.listdir("paragraphes")
    filename = "paragraphes/"+files[random.randint(0,len(files)-1)]
    print(filename)
    content = ""
    with open(filename) as f:
        content = f.read().splitlines()

    #print(content)
    # 2 => number of paragraphe to receive
    text = splitSize(content[0])
    players[:playersCurrents] = broadcast(socket, "002"+str(len(text)), players)

    # 3 => each part of the paragraphe
    for i in range(len(text)):
        players[:playersCurrents] =broadcast(socket, "003"+text[i], players)

    # For a game:
    time.sleep(timeGame)
    # 4 => End game and number of score to send
    msg = "004"+str(len(gameArray))
    players[:playersCurrents] = broadcast(socket, msg, players[:playersCurrents])

    # 5 => score
    for i in range(len(gameArray)):
        # PlayerID, IDplayersArray, time, score
        msg = "005"+players[gameArray[i][1]][1]+ " " + str(gameArray[i][3])
        players[:playersCurrents] = broadcast(socket, msg, players[:playersCurrents])

    # 6 => Winner or Loser
    max = 0
    for i in range(len(gameArray)):
        # max score
        #print(str(int(gameArray[i][3]) > int(gameArray[max][3])) + " " + str(int(gameArray[i][3])) + " " + str(int(gameArray[max][3])))
        if int(gameArray[i][3]) > int(gameArray[max][3]):
            max = i

    # PlayerID, IDplayersArray, time, score
    msg = "006"+gameArray[max][0]
    players[:playersCurrents] = broadcast(socket, msg, players[:playersCurrents])

    # 7 => New game !
    msg = "007ready for a new one?"
    players[:playersCurrents] = broadcast(socket, msg, players[:playersCurrents])

if __name__ == '__main__':
    HOST = "127.0.0.1" # Machine addr
    PORT_TCP = 6666
    PORT_UDP = 6667

    # TCP connection and listenning
    threading.Thread(target=servConnectionTCP, args=(HOST, PORT_TCP,)).start()

    # MAINS function
    udpSocket = servConnectionUDP(HOST, PORT_UDP)

    # UDP messages process
    threading.Thread(target=UDP_listen, args=(udpSocket,)).start()
    while True:
        game(udpSocket)
        # TODO still alive
